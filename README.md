# get-facebook-5-star-ratings-embeds

Creates an HTML file with all your facebook page's 5-stars ratings (old rating system) and positive recommendations as iframes containing the review's post, so you can easily embed them in any web page.

```
usage: get-facebook-5-star-ratings-embeds [-h] [-v] [-o OUTPUT_FILE] -t
                                          ACCESS_TOKEN -i FB_PAGE_ID [-s]
                                          [-W WIDTH] [-H HEIGHT]


Creates an HTML file with all your facebook page's 5-stars ratings (old
rating system) and positive recommendations as iframes containing the review's
post, so you can easily embed them in any web page

Optional arguments:
  -h, --help            Show this help message and exit.
  -v, --version         Show program's version number and exit.
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        path to the file that will store the resulting with
                        all the "5-stars" ratings in HTML format to be
                        embedded in any web page. Default = .
                        /embeddable-ratings.html
  -t ACCESS_TOKEN, --access-token ACCESS_TOKEN
                        Facebook Access Token for the Facebook API request
                        that will get the list of ratings
  -i FB_PAGE_ID, --fb-page-id FB_PAGE_ID
                        Facebook Page ID of the page you want to get the
                        ratings from
  -s, --std-out         Print in stdout and not on file. Overrides
                        --output-file
  -W WIDTH, --width WIDTH
                        Width for the iframe that will contain the review
                        post. Default = 500
  -H HEIGHT, --height HEIGHT
                        Height for the iframe that will contain the review
                        post. Default = 500

```

As you can see, you must provide:

* A valid Facebook API **access token**. It has to be a Page Access Token with *manage_pages* permission.
* The Facebook Page **ID** (you can get that number at `https://www.facebook.com/pg/<your_facebook_page_name>/about`).

Optionally, you can indicate the path and file name, for the resulting CSV file (e.g. ~/Documents/my-page-ratings.csv).

You can also (optionally) indicate also a width and height for the iframes. And, if you don't want to create a file and get the results printed in the console, you can add the `-s` flag.

For more information: [https://developers.facebook.com/docs/graph-api/reference/page/ratings/](https://developers.facebook.com/docs/graph-api/reference/page/ratings/).

**Note**: I recommend you adjust the height of every resulting iframe after running this. Some posts could be cropped while others could be much smaller and using too much blank vertical space.

## Why?

Because sometimes you need those "sweet" top reviews as social proof for marketing campaigns, sales processes, promotion, etc.

And embedding them on your web page is a nice way of showcasing that "social proof".
