#!/usr/bin/env node
'use strict';

const pack = require('./package.json');
const fs = require('fs');
const { getRatings, filterRatings } = require('fb-ratings');
const { ArgumentParser } = require('argparse');
const parser = new ArgumentParser({
  version: pack.version,
  addHelp:true,
  description: pack.description,
  prog: pack.name
});
parser.addArgument(
  [ '-o', '--output-file' ],
  {
    help: 'path to the file that will store the resulting with all the "5-stars" ratings in HTML format to be embeded in any web page. Default = ./embeddable-ratings.html',
    defaultValue: './embeddable-ratings.html',
  },
);
parser.addArgument(
  [ '-t', '--access-token' ],
  {
    help: 'Facebook Access Token for the Facebook API request that will get the list of ratings',
    required: true,
  },
);
parser.addArgument(
  [ '-i', '--fb-page-id' ],
  {
    help: 'Facebook Page ID of the page you want to get the ratings from',
    required: true,
  },
);
parser.addArgument(
  [ '-s', '--std-out' ],
  {
    help: 'Print in stdout and not on file. Overrides --output-file',
    action: 'storeTrue',
  },
);
parser.addArgument(
  [ '-W', '--width' ],
  {
    help: 'Width for the iframe that will contain the review post. Default = 500',
    defaultValue: '500',
  },
);
parser.addArgument(
  [ '-H', '--height' ],
  {
    help: 'Height for the iframe that will contain the review post. Default = 500',
    defaultValue: '500',
  },
);
const args = parser.parseArgs();
const accessToken = args.access_token;
const fbPageId = args.fb_page_id;
const outputFile = args.output_file;
const stdOut = args.std_out;
const width = args.width;
const height = args.height;

getRatings(accessToken, fbPageId, []).then(ratings => {
  let embedUrls = [];
  let src = filterRatings(ratings)
    .map(rating => `https://www.facebook.com/${rating.reviewer.id}/posts/${rating.open_graph_story.id}`)
    .map(url => encodeURIComponent(url))
    .map(url => `<iframe src="https://www.facebook.com/plugins/post.php?href=${url}%3A0&amp;width=${width}" width="${width}" height="${height}" style="border: none; overflow: hidden;" scrolling="no" frameborder="0" allowtransparency="true"></iframe>`)
    .join('\n');
  if(stdOut) {
    console.log(src);
  } else {
    fs.writeFile(outputFile, src, err => {
        if (err) console.log(err);
        else console.log(`embeddable ratings saved in file ${outputFile}`);
    });
  }
}, error => console.log(error));
